window.addEventListener("load", function () {

    /**
     * Button that starts/stops the timer
     * @type {Element}
     */
    const timerToggleButton = document.querySelector("#run")
    timerToggleButton.classList.add("button_enabled");

    /**
     * Button the resets the timer
     * @type {Element}
     */
    const resetButton = document.querySelector("#rst")
    resetButton.classList.add("button_disabled");

    let timerId;
    let autoResetId;
    let elapsed = 0;

    timerToggleButton.addEventListener("click", function () {
        if (timerToggleButton.innerText === "Start") {
            startTimer();
        } else {
            stopTimer();
        }
    });

    resetButton.addEventListener("click", function() {
        let isEnabled = false;
        resetButton.classList.forEach(function(className) {
            if (className === "button_enabled") {
                isEnabled = true;
            }
        });

        if (isEnabled) {
            resetTimer();
        }
    });

    /**
     * Starts the timer
     */
    function startTimer() {
        resetButton.classList.replace("button_enabled", "button_disabled");
        timerToggleButton.innerText = "Stop";

        if (autoResetId !== undefined) {
            clearTimeout(autoResetId);
            autoResetId = undefined;
        }

        // this function is only called when the timer is stopped, so we don't need to test if an interval is already running
        timerId = setInterval(function () {
            elapsed++;
            const display = document.querySelector("#timer_face");
            if (display.innerText !== `${elapsed / 10}`) {
                display.innerText = (elapsed / 10).toFixed(1);  // so that 1 displays as  1.0
            }

        }, 100);
    }

    /**
     * Stops the timer and silently begins a 30s countdown to reset the timer face if no action taken
     */
    function stopTimer() {
        timerToggleButton.innerText = "Start";
        resetButton.classList.replace("button_disabled", "button_enabled");

        // just stopping, not resetting timer (yet), so can't use the resetTimer function
        clearInterval(timerId);
        autoResetId = setTimeout(resetTimer, 30000);
    }

    /**
     * Resets the timer face to 0.0 and disables the reset button
     */
    function resetTimer() {
        document.querySelector("#timer_face").innerText = "0.0";
        resetButton.classList.replace("button_enabled", "button_disabled");
        elapsed = 0;
        clearTimeout(timerId);
        timerId = undefined;
    }


    // create some html elements to hold the parts of the clock to make for easier updating
    document.querySelector("#clock_face").innerHTML = "<span id='hours'></span><span id='separator'>:</span><span id='minutes'></span> <span id='amPm'></span>"
    const hours = document.querySelector("#hours");
    const separator = document.querySelector("#separator");
    const minutes = document.querySelector("#minutes");

    const amPm = document.querySelector("#amPm");
    const now = new Date();
    updateDate(now);
    //setHours changes both the hour and the AM/PM indicate so is its own function rather than just changing the element directly)
    setHours(getHours(now));
    minutes.innerText = getMinutes(now);

    // clock face updater
    setInterval(function() {
        const now = new Date();

        if (separator.innerText === ":") {
            separator.innerText = " ";
        } else {
            separator.innerText = ":";
        }

        if (now.getSeconds() === 0) {
            minutes.innerText = getMinutes(now);

            if (now.getMinutes() === 0) {
                setHours(getHours(now));

                if (now.getHours() === 0) {
                    updateDate(now);
                }
            }
        }

    }, 1000);

    /**
     * Pads a number to 2 chars with 0
     * @param number number to pad
     * @returns {string|*} padded number
     */
    function padTime(number) {
        if (number < 10) {
            return `${number}`.padStart(2, '0');
        }

        return number;

    }

    /**
     * Updates the date on the clock face in the format DDDD, D MMMM YYYY
     * @param now Date object containing the current time
     */
    function updateDate(now) {
        const dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'}

        const dateDisplay = document.querySelector("#date_face");
        dateDisplay.innerText = now.toLocaleString('en-NZ', dateOptions);
    }

    /**
     * Returns the current hour on a 12 hour clock, padded to 2 chars with 0
     * @param now Date object containing the current time
     * @returns {{hours: string | number, amPm: string}}
     */
    function getHours(now) {
        let hours = padTime(now.getHours());
        let amPm = "AM";
        if (hours > 12) {
            hours = padTime(hours - 12);
            amPm = "PM";
        } else if (now.getHours() === 0) {
            hours = 12;
        }
        return { hours: hours, amPm: amPm };
    }

    /**
     * Sets the hour and am/pm indicate on the clock face
     * @param hourInfo json object with the properties hours and amPm
     */
    function setHours(hourInfo) {
        hours.innerText = hourInfo.hours;
        amPm.innerText = hourInfo.amPm;
    }

    /**
     * Returns the current minutes past the hour padded to 2 chars with 0
     * @param now Date object containing the current time
     * @returns {string} minutes past the hour padded to 2 chars with 0
     */
    function getMinutes(now) {
        return padTime(now.getMinutes());
    }
});
